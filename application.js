feather.replace();

$(document).ready(function(){

    $('.swatch-option-selector').change(function (){

        $('.swatch-label').removeClass('swatch-active');
		$('.swatch-option-selector:checked').next().addClass('swatch-active');

	});

    {% if settings.options.recent_sales %}
        // Recent sales popup
        $.getJSON('/recent.json', function(data){
            var proofs = [];

            $.each(data, function(key, val){
                var customerName = ((val.customer.name) ? val.customer.name : ('{{ settings.options.recent_name }}') ? '{{ settings.options.recent_name }}' : 'Someone'),
                    productDetails = val.product.name,
                    orderTimeAgo = '',
                    customerCountry = val.customer.country;

                if(customerCountry === 'Malaysia'){
                    customerCountry = '🇲🇾';
                }else if(customerCountry === 'Brunei'){
                    customerCountry = '🇧🇳';
                }else if(customerCountry === 'Singapore'){
                    customerCountry = '🇸🇬';
                }else if(customerCountry === 'Indonesia'){
                    customerCountry = '🇮🇩';
                }else{
                    customerCountry = '';
                }

                var customerLocation = ((val.customer.city) ? val.customer.city + ', ' : '') + ((val.customer.state) ? val.customer.state + ' ' : '') + customerCountry;

                customerLocation = ((customerLocation != '') ? ' from ' + customerLocation : '');

                var totalOrderPrice = val.total_order_price,
                    match = totalOrderPrice.match(/([^\d]+)([\d\.]+)/),
                    isDisplayFreeTextAtZeroPrice = {{ settings.options.display_product_price_to_free == true ? 'true' : 'false' }};

                    if(match){
                        var currency = match[1],
                            amount = match[2];
                    }
                if(amount == 0.00 && isDisplayFreeTextAtZeroPrice){
                    totalOrderPrice = "Free";
                } 
                    
                {% if settings.options.recent_sales_total_order_price %}
                    productDetails = val.product.name + ' ' + totalOrderPrice;
                {% endif %}

                {% if settings.options.recent_sales_order_timeago %}
                    orderTimeAgo = '<p class="mb-0 mt-0 small"><span class="time">' + jQuery.timeago(val.created_at) + '</span></p>';
                {% endif %}

                proofs.push('<div class="social-proof d-none"><div class="box-content"><div class="row mx-0 align-items-center"><div class="col-12 d-flex px-0"><div class="notification-content d-flex flex-column justify-content-center"><p class="mb-0">' + customerName + customerLocation + ' recently ordered</p><a class="product-link" href="' + val.product.url +'">' + productDetails + '</a>' + orderTimeAgo +'<p class="mb-0"><span class="small text-muted"><svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check-circle" style="color: #28a745"><path d="M22 11.08V12a10 10 0 1 1-5.93-9.14"></path><polyline points="22 4 12 14.01 9 11.01"></polyline></svg> Verified purchase by Shoppego</span></p></div><div class="notification-close d-inline-flex"><svg width="10px" height="10px" viewBox="0 0 48 48"><g stroke="none"><g><path d="M28.228 23.986L47.092 5.122a2.998 2.998 0 000-4.242 2.998 2.998 0 00-4.242 0L23.986 19.744 5.121.88a2.998 2.998 0 00-4.242 0 2.998 2.998 0 000 4.242l18.865 18.864L.879 42.85a2.998 2.998 0 104.242 4.241l18.865-18.864L42.85 47.091a2.991 2.991 0 002.121.879 2.998 2.998 0 002.121-5.121L28.228 23.986z"></path></g></g></svg></div></div></div></div></div>');
	        });

            $('.social-proofs').append(proofs);

            var socialProofs = $('.social-proofs').find('.social-proof'),
                index = 0,
                isRunning = true,
                intervalId;

            setTimeout(function(){

                socialProofs.eq(index).removeClass('d-none').addClass('slide-in');

                intervalId = setInterval(function (){
                    if(!isRunning){
                        clearInterval(intervalId); // Stop the interval if isRunning is false
                        return;
                    }

                    socialProofs.eq(index).removeClass('slide-in').addClass('d-none');

                    setTimeout(function (){
                        socialProofs.eq(index).addClass('d-none');
                        index = (index + 1) % socialProofs.length;
                        socialProofs.eq(index).removeClass('d-none').addClass('slide-in');
                    }, 1000); // 1 seconds delay before show other popup
                }, 6000); // 3 seconds visible + 3 second delay
            }, 5000); // 5 seconds delay for 1st popup

            $('.social-proof .notification-close').on('click', function() {
                isRunning = false; // Stop the animation loop when close button is clicked
                $('.social-proof').addClass('d-none'); // Hide social proof
            });

        });
    {% endif %}

    // Cart upsell
    $('#cart_upsell_btn').click(function(){
        $('#cartupsell').submit();
    });

    $('#carousel-gallery').find('.carousel-inner').find(':first-child').addClass('active');

    // Clear form on window load
    $(window).on('load', function(){
        clearForm();
    });

    // Init carousel
    $('#carousel-gallery').carousel({
        interval: false,
        pause: true
    });

    // Hover carousel
    $('.carousel-thumbnails li').hover(function(){
        var goto = Number($(this).attr('data-slide-to'));
        $('.carousel-thumbnails').carousel(goto);
    });

    // Radio button is the variant
    $('.single-option-selector__radio.radio-variant').change(function(){
        var variantId = $(this).val(), selectedVariant = $('#variant_' + variantId);

        disabledButtons();
        resetInputQuantity(0);
        updateThumbnailImage(variantId);
        $('#max-product-quantity').text(0);
        $('#max-product-quantity').attr('value', 0);

        if(selectedVariant.data('available')){
            updateSelectedOptionsVariant(variantId);// must update variant first
            enableButtons();
            resetInputQuantity(1);
            updatePriceText();
            updateQuantityText();
            showQuantityText();
        }
    });

    // Have option value
    $('.option').change(function(){
        var optionsLength = $($('#list-variants-options').find('div')[0]).children().length;
        var chooseOptions = $('.option:checked, .option option:selected');
        var currentCheckedRadio, optionName, optionKey, optionValue, chooseOptionsLength = chooseOptions.length, selectedOptions = {};

        // If same length, means that customer have choose each options
        if(optionsLength == chooseOptionsLength){
            chooseOptions.each(function(){
                currentCheckedRadio = $(this).is('option') ? $(this).parent() : $(this);

                optionName = currentCheckedRadio.attr('name');
                optionKey = optionName.split(/\[|\]/)[1];
                optionValue = currentCheckedRadio.val();

                selectedOptions[optionKey] = optionValue;
            });

            processSelectOptions(selectedOptions);
        }
    });

    // Bundle selection
    $('.single-bundle-selector__select.select-bundle').change(function(){
        var selectBoxes = $('.single-bundle-selector__select.select-bundle'),
        variantId = $(this).val(),
        productId = $(this).prev().val(),
        loopId = $(this).prev().data('loop'),
        selectedVariant = $('#variant_' + variantId + '_' + loopId),
        allSelectionsAvailable = true;

        disabledBundleButtons();
        updateBundleAvailabilityText(productId, variantId, loopId);
        resetBundleInputQuantity(productId, 0, loopId);
        checkBundleSelect(productId, loopId);
        updateBundleThumbnailImage(productId, variantId, loopId);
        updateBundleFixQuantity(productId, variantId, loopId);
        updateBundlePriceText(productId, variantId, loopId);

        selectBoxes.each(function(){
            var eachVariantId = $(this).val(),
            eachLoopId = $(this).prev().data('loop'),
            eachSelectedVariant = $('#variant_' + eachVariantId + '_' + eachLoopId),
            eachCheckAvailable = eachSelectedVariant.data('available');

            if(!eachCheckAvailable){
                allSelectionsAvailable = false;
                return false;
            }
        });

        if(selectedVariant.data('available')){
            resetBundleInputQuantity(productId, 1, loopId);
            updateSelectedBundleVariant(productId, variantId, loopId);// must update variant first
            updateBundlePriceText(productId, variantId, loopId);
        }
        if(allSelectionsAvailable){
            enableButtons();
        }
    });

    // Plus quantity on click
    $('#product_plus_quantity').on('click', function(){
        plusQuantity();
    });

    // Minus quantity on click
    $('#product_minus_quantity').on('click', function(){
        minusQuantity();
    });

    // Bundle plus quantity on click
    $('.bundle-quantity-button.bundle-plus').on('click', function(){
        var productId = $(this).data('product-id'),
        loopId = $(this).data('loop'),
        selectedProductVariant = $('#product_bundle_' + productId + '_variant_' + loopId).val(),
        selectedVariant = $('#variant_' + selectedProductVariant + '_' + loopId);

        updateBundleAvailabilityText(productId, selectedProductVariant, loopId);
        checkBundleSelect(productId, loopId);

        if(selectedVariant.data('available')){
            plusBundleQuantity(productId, loopId);
        }
    });

    // Bundle minus quantity on click
    $('.bundle-quantity-button.bundle-minus').on('click', function(){
        var productId = $(this).data('product-id'),
        loopId = $(this).data('loop'),
        selectedProductVariant = $('#product_bundle_' + productId + '_variant_' + loopId).val(),
        selectedVariant = $('#variant_' + selectedProductVariant + '_' + loopId);

        updateBundleAvailabilityText(productId, selectedProductVariant, loopId);
        checkBundleSelect(productId, loopId);

        if(selectedVariant.data('available')){
            minusBundleQuantity(productId, loopId);
        }
    });

    // Quantity input box is on type in value
    $('#product_input_quantity').on('keyup', function(event){
        inputQuantity(event);
    });

    // Bundle Quantity input box is on type in value
    $('.bundle-quantity-button.bundle-quantity-form').on('keyup', function(event){
        var productId = $(this).data('product-id'),
        loopId = $(this).data('loop'),
        selectedProductVariant = $('#product_bundle_' + productId + '_variant_' + loopId).val(),
        selectedVariant = $('#variant_' + selectedProductVariant + '_' + loopId);

        updateBundleAvailabilityText(productId, selectedProductVariant, loopId);
        checkBundleSelect(productId, loopId)

        if(selectedVariant.data('available')){
            inputBundleQuantity(productId, loopId);
        }
    });

    // When click the search icon modal button
	$('#searchIconModal').on('click', function(){
		getHeaderHeight();
    });

    // When click the search icon modal menu collapse button
	$('#searchIconModalCollapse').on('click', function(){
		getHeaderHeight();
    });

	// If Search box modal shown will autofocus input
	$('#modalSearch').on('shown.bs.modal', function(){
		$('#products-search-input').focus();
	});

	// When press enter at search textbox
	$('#products-search-input').keyup(function(event){
        if(event.keyCode === 13){
            searchFunction();
        }
    });

	// When click the search icon button
	$('#buttonSearch').on('click', function(){
		searchFunction();
    });

	var searchValue = new URLSearchParams(window.location.search);
	if(searchValue.has('q') == true){
		var currentSeachValue = searchValue.get('q');
		$('#searchValueProducts').text(currentSeachValue);
		$('#searchResult').removeClass('d-none');
		$('#products-search-input').val(currentSeachValue);
	}

    $('#myNavbar .dropdown-menu').on('click', function(e){
        e.stopPropagation();
    });

    $('#myNavbar .dropdown-menu a').on("click", function(e){
        if($(this).next('.submenu').length){
            $(this).next('.submenu').toggle();

            var submenu = $(this).next('.submenu').css('display');

            if(submenu == 'none'){
                $(this).attr('aria-expanded', 'false');
            }else{
                $(this).attr('aria-expanded', 'true');
            }
        }
    });

    // When click any of the paginate button
    $('.paginate-btn').on('click', function(e){
        e.preventDefault();

        var params = new URLSearchParams(window.location.search);
        var qValue = params.get('q');
        var clickedUrl = $(this).attr('href'); // Get paginate button href link

        // If qValue is not null
        if(qValue){
            // Check if clickedUrl has other parameters
            if(clickedUrl.indexOf('?') !== -1){
                // If true, append &q=a instead
                clickedUrl += '&q=' + qValue;
            }else{
                clickedUrl += '?q=' + qValue;
            }
        }

        window.location.href = clickedUrl;
    });
});

function getHeaderHeight(){
    var headerHeight = $('section.header').outerHeight(),
        announcementHeight = $('div.announcement-bar').outerHeight(),
        height = 0;

        height = headerHeight + announcementHeight;
    
    $('#modalSearch .modal-content').height(height);
}

function searchFunction(){
	var searchBoxValue = $('#products-search-input').val();
	$('#buttonSearch').attr('href', '/products?q=' + searchBoxValue);
	$('#buttonSearch')[0].click();
}

function processSelectOptions(selectedObject){
    var div, key, value, selected_variant, found = [];
    var list_variant = $('#list-variants-options');

    list_variant.find('div').each(function(index, element){
        div = $(element), found[div.data('variant')] = [];
        div.find('input').each(function(index, input){
            key = $(input).data('optionid'),
            value = $(input).val();

            found[div.data('variant')].push((selectedObject[key] === value));
        });
    });

    for(var variantId in found){
        disabledButtons();
        showQuantityText(false);
        resetInputQuantity(1);
        $('#max-product-quantity').text(1);
        $('#max-product-quantity').attr('value', 1);

        if(found[variantId].includes(false) === false){
            selected_variant = $("#variant_" + variantId);

            updateSelectedOptionsVariant(variantId); // update selected variant first
            updateThumbnailImage(variantId);
            updatePriceText(variantId);
            disabledButtons();

            if(selected_variant.data('available')){
                enableButtons();
                updatePriceText();
                updateQuantityText();
                showQuantityText();
            }

            break;
        }else{ disabledButtons(); }
    }
    return;
}

function updateSelectedOptionsVariant(variantId){
    $('input#selected-option[name="id"]').attr('value', variantId);
}

function updateSelectedBundleVariant(productId, variantId, loopId){
    $('input#product_bundle_' + productId + '_variant_' + loopId).attr('value', variantId);
}

function updateThumbnailImage(variantId){
    var selectedVariantImage = $("#img_variant" + variantId),
        image_gallery = $('#carousel-gallery');

    // Clear/remove current active
    image_gallery.find('div.carousel-item.active').removeClass('active');

    if(selectedVariantImage.length > 0){
        selectedVariantImage.parent().addClass('active');
    }else{
        image_gallery.find('div.carousel-item').first().addClass('active');
    }
}

function updateBundleFixQuantity(productId, variantId, loopId){
    var selectedBundleRow = $('#row_' + productId + '_' + loopId),
    selectedVariant = $('#variant_' + variantId + '_' + loopId),
    selectedFixQuantityElement = $('#fixed_quantity_' + loopId),
    bundleFixQuantity = selectedVariant.data('fixed-quantity');

    if(bundleFixQuantity == '' || bundleFixQuantity == null || (typeof bundleFixQuantity === 'undefined')){
        selectedFixQuantityElement.addClass('d-none').removeClass('d-block');
        selectedFixQuantityElement.text('');
        $('input[name="bundle_products[' + loopId + '][product_id]"]').parent('.bundle-variants').addClass('mb-2');

        if(variantId == ''){
            selectedBundleRow.find('.input-quantity').addClass('d-none').removeClass('d-block');
        }else{
            selectedBundleRow.find('.input-quantity').addClass('d-block').removeClass('d-none');
        }
    }else{
        selectedFixQuantityElement.removeClass('d-none');
        selectedFixQuantityElement.text(bundleFixQuantity);
        $('input[name="bundle_products[' + loopId + '][product_id]"]').parent('.bundle-variants').removeClass('mb-2');

        selectedBundleRow.find('.input-quantity').addClass('d-none').removeClass('d-block');
    }
}

function updateBundleThumbnailImage(productId, variantId, loopId){
    var selectedProductBundleImage = $('#img_bundle_' + productId + '_' + loopId),
    selectedVariantBundleImage = $('#img_bundle_' + variantId + '_' + loopId);

    if(variantId == '' || variantId == null || (typeof variantId === 'undefined')){
        variantId = $('#product_bundle_' + productId + '_' + 'variant_' + loopId).val(),
        selectedVariantBundleImage = $('#img_bundle_' + variantId + '_' + loopId);
    }

    var rowElement = $('#row_' + productId + '_' + loopId),
    bundleThumbnailElement = rowElement.find('.bundle-thumbnail').find('[id^="img_bundle_"]'),
    selectedVariant = $('#variant_' + variantId + '_' + loopId),
    image = selectedVariant.data('image');

    bundleThumbnailElement.addClass('d-none').removeClass('d-block');

    if(image == '' || (typeof image === 'undefined')){
        selectedVariantBundleImage = $('#img_bundle_empty_' + variantId + '_' + loopId);

        selectedProductBundleImage.addClass('d-block').removeClass('d-none');
        selectedVariantBundleImage.addClass('d-block').removeClass('d-none');
    }else{
        selectedVariantBundleImage.addClass('d-block').removeClass('d-none');
    }
}

function updatePriceText(variantId = null){
    var selectedVariant = $('#variant_' + $('#selected-option').val()),
        currentQuantity = $('#product_input_quantity').val(),
        wholesaleInput = $('input[id^="wholesalePrice_"]'),
        formattedProductPrice, formattedDiscountPrice,
        formattedTotalPrice, formattedTotalPriceAtome, productPrice, formattedDisplayTotalPrice, formattedDisplayTotalDiscountPrice, formattedDisplayTotalPriceAtome, discountPrice,
        productPriceElement, discountPriceElement, productPriceElementAtome, currencyType, storeCurrency, fixedSize, totalPrice = 0, totalDiscountPrice = 0, haveWholesale = 'false', totalPriceAtome = 0,
        isDisplayProductFreeTextAtZeroPrice = {{ settings.options.display_product_price_to_free == true ? 'true' : 'false' }};

        // override if variantId not null
        if(variantId){
            selectedVariant = $('#variant_' + variantId);
        }

        if(wholesaleInput.length >= 1){
            haveWholesale = 'true';
        }

        // if input quantity box not exist, set default to 1
        currentQuantity = currentQuantity ? currentQuantity : 1;

        //Get Store currency
        storeCurrency = $('#product-price').data('currency');
        fixedSize = (storeCurrency === 'IDR') ? 0 : 2;

        formattedDiscountPrice = selectedVariant.data('discount-price');
        formattedProductPrice = selectedVariant.data('price');
        currencyType = formattedProductPrice.match(/^\D+/g)[0];
        

        if(haveWholesale === 'true'){
            wholesaleInput.each(function(){
                var minQuantity = $(this).data('min-quantity'),
                    maxQuantity = $(this).data('max-quantity');

                if(currentQuantity >= minQuantity && (!maxQuantity || currentQuantity <= maxQuantity)){
                    formattedProductPrice = $(this).data('price');
                    currencyType = $(this).data('currency');
                    return false;
                } 
            });

        }

        productPrice = formattedProductPrice.replace(/^\D+/g, '').replace(/,/g, '');
        discountPrice = formattedDiscountPrice.replace(/^\D+/g, '').replace(/,/g, '');

        productPriceElement = $('#product-price');
        productPriceElementAtome = $('#product-price-atome');
        discountPriceElement = $('#discount-price-single');

        totalPrice = productPrice * currentQuantity;
        formattedDisplayTotalPrice = 0;

        if(totalPrice == 0 && isDisplayProductFreeTextAtZeroPrice){
            formattedDisplayTotalPrice = "Free";
        }else{
            formattedTotalPrice = totalPrice.toFixed(fixedSize);

            //Format total price with comma
            if(parseFloat(totalPrice) >= 1000){
                formattedDisplayTotalPrice = formattedTotalPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                formattedDisplayTotalPrice = currencyType + formattedDisplayTotalPrice;
            }else{
                formattedDisplayTotalPrice = currencyType + formattedTotalPrice;
            }
        }
        
        totalPriceAtome = (productPrice * currentQuantity) / 3;
        formattedTotalPriceAtome = totalPriceAtome.toFixed(fixedSize);

        //Format total price Atome with comma
        if(parseFloat(totalPriceAtome) >= 1000){
            formattedDisplayTotalPriceAtome = formattedTotalPriceAtome.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            formattedDisplayTotalPriceAtome = currencyType + formattedDisplayTotalPriceAtome;
        }else{
            formattedDisplayTotalPriceAtome = currencyType + formattedTotalPriceAtome;
        }

        // Set the price
        productPriceElement.text(formattedDisplayTotalPrice);
        productPriceElementAtome.text(formattedDisplayTotalPriceAtome);

        // If variant is available and have discount price, set the price. Otherwise clear the text.
        if(selectedVariant.data('available') && formattedDiscountPrice){
            totalDiscountPrice = discountPrice * currentQuantity;
            formattedTotalDiscountPrice = totalDiscountPrice.toFixed(fixedSize);

            //Format total price with comma
            if(parseFloat(totalDiscountPrice) >= 1000){
                formattedDisplayTotalDiscountPrice = formattedTotalDiscountPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                formattedDisplayTotalDiscountPrice = currencyType + formattedDisplayTotalDiscountPrice;
            }else{
                formattedDisplayTotalDiscountPrice = currencyType + formattedTotalDiscountPrice;
            }

            discountPriceElement.text(formattedDisplayTotalDiscountPrice);
            discountPriceElement.removeClass('mr-0');
        }else{
            discountPriceElement.text('');
            discountPriceElement.addClass('mr-0');
        }
}

function updateBundlePriceText(productId, bundleVariantId = null, loopId){
    var selectedProductVariant = $('#product_bundle_' + productId + '_variant_' + loopId).val(),
        selectedVariant = $('#variant_' + selectedProductVariant + '_' + loopId ),
        input_quantity_box = $('input[data-product-id="'+ productId +'"][data-loop="'+ loopId +'"]'),
        currentQuantity = parseInt(input_quantity_box.attr('value')),
        bundlePriceSection = $('#' + bundleVariantId + '_' + loopId + '.bundle-price-variants').parent('.bundles-price'),
        bundlePriceDiv = bundlePriceSection.find('.bundle-price-variants'),
        productBundleDiscountPrice = calculateTotalVisibleComparePrice(),
        totalBundlesPrice = calculateTotalVisiblePrice(),
        bundlePrice, bundleDiscountPrice,
        formattedProductBundlePrice, formattedProductBundleDiscountPrice, formattedBundlePrice, formattedBundleDiscountPrice,
        productBundlePriceElement, productBundleDiscountPriceElement,
        formattedBundleTotalPrice, formattedTotalBundleDiscountPrice, formattedTotalBundlePriceAtome, formattedTotalBundlesSavedPrice, formattedTotalBundleSavedPrice, formattedDisplayProductBundlePrice, formattedDisplayProductBundleDiscountPrice, formattedDisplayBundleTotalPrice, formattedDisplayTotalBundleDiscountPrice, formattedDisplayTotalBundlePriceAtome, formattedDisplayTotalBundlesSavedPrice, formattedDisplayTotalBundleSavedPrice,
        bundlePriceElement, bundleDiscountPriceElement, bundlePriceElementAtome, bundlesSavedPriceElement, bundleSavedPriceElement, currencyType, storeCurrency, fixedSize, totalBundlePrice = 0, totalBundleDiscountPrice = 0, totalBundlePriceAtome = 0, totalBundlesSavedPrice = calculateSavedPrice(), totalBundleSavedPrice = 0;

        $(bundlePriceDiv).removeClass('d-block').addClass('d-none');
        $(bundlePriceDiv).next('[id^="bundle-saved-price_"]').removeClass('d-block').addClass('d-none');
        $('#' + bundleVariantId + '_' + loopId + '.bundle-price-variants').removeClass('d-block').addClass('d-none');
        $('#' + bundleVariantId + '_' + loopId).removeClass('d-none').addClass('d-block');
        $('#bundle-saved-price_' + bundleVariantId + '_' + loopId).removeClass('d-none').addClass('d-block');

        // override if variantId not null
        if(bundleVariantId){
            selectedVariant = $('#variant_' + bundleVariantId + '_' + loopId );
        }

        formattedBundlePrice = selectedVariant.data('price');

        formattedBundleDiscountPrice = selectedVariant.data('discount-price');

        bundlesPrice = formattedBundlePrice.replace(/^\D+/g, '').replace(/,/g, '');
        currencyType = formattedBundlePrice.match(/^\D+/g)[0];
        bundlePrice = formattedBundlePrice.replace(/^\D+/g, '').replace(/,/g, '');
        bundleDiscountPrice = formattedBundleDiscountPrice.replace(/^\D+/g, '').replace(/,/g, '');

        //Get Store currency
        storeCurrency = $('#product-price').data('currency');
        fixedSize = (storeCurrency === 'IDR') ? 0 : 2;

        // if input quantity box not exist, set default to 1
        currentQuantity = currentQuantity ? currentQuantity : 1;

        productBundlePriceElement = $('#product-price');
        productBundleDiscountPriceElement = $('#discount-price-single');
        bundlePriceElement = $('#bundle-price_' + bundleVariantId + '_' + loopId);
        bundlePriceElementAtome = $('#product-price-atome');
        bundleDiscountPriceElement = $('#bundle-discount-price-single_' + bundleVariantId + '_' + loopId);
        bundlesSavedPriceElement = $('#bundle-saved-price');
        bundleSavedPriceElement = $('#bundle-saved-price_' + bundleVariantId + '_' + loopId);

        totalBundlePrice = bundlePrice * currentQuantity;
        totalBundlesPrice = totalBundlesPrice;
        totalBundleDiscountPrice = bundleDiscountPrice * currentQuantity;
        totalBundleSavedPrice = totalBundleDiscountPrice - totalBundlePrice;
        totalBundlePriceAtome = totalBundlesPrice / 3;

        if(productBundleDiscountPrice < 0){
            productBundleDiscountPrice = 0;
        }

        if(totalBundlesSavedPrice < 0){
            totalBundlesSavedPrice = 0;
        }

        formattedProductBundlePrice = totalBundlesPrice.toFixed(fixedSize);
        formattedProductBundleDiscountPrice = productBundleDiscountPrice.toFixed(fixedSize);
        formattedBundleTotalPrice = totalBundlePrice.toFixed(fixedSize);
        formattedTotalBundleDiscountPrice = totalBundleDiscountPrice.toFixed(fixedSize);
        formattedTotalBundlePriceAtome = totalBundlePriceAtome.toFixed(fixedSize);
        formattedTotalBundlesSavedPrice = totalBundlesSavedPrice.toFixed(fixedSize);
        formattedTotalBundleSavedPrice = totalBundleSavedPrice.toFixed(fixedSize);

        //Format total bundles price with comma
        if (parseFloat(totalBundlesPrice) >= 1000){
            formattedDisplayProductBundlePrice = formattedProductBundlePrice.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            formattedDisplayProductBundlePrice = currencyType + formattedDisplayProductBundlePrice;
        }else{
            formattedDisplayProductBundlePrice = currencyType + formattedProductBundlePrice;
        }

        //Format product bundle discount price price with comma
        if (parseFloat(productBundleDiscountPrice) >= 1000){
            formattedDisplayProductBundleDiscountPrice = formattedProductBundleDiscountPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            formattedDisplayProductBundleDiscountPrice = currencyType + formattedDisplayProductBundleDiscountPrice;
        }else{
            formattedDisplayProductBundleDiscountPrice = currencyType + formattedProductBundleDiscountPrice;
        }

        //Format total bundle price with comma
        if (parseFloat(totalBundlePrice) >= 1000){
            formattedDisplayBundleTotalPrice = formattedBundleTotalPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            formattedDisplayBundleTotalPrice = currencyType + formattedDisplayBundleTotalPrice;
        }else{
            formattedDisplayBundleTotalPrice = currencyType + formattedBundleTotalPrice;
        }

        //Format total bundle discount price with comma
        if (parseFloat(totalBundleDiscountPrice) >= 1000){
            formattedDisplayTotalBundleDiscountPrice = formattedTotalBundleDiscountPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            formattedDisplayTotalBundleDiscountPrice = currencyType + formattedDisplayTotalBundleDiscountPrice;
        }else{
            formattedDisplayTotalBundleDiscountPrice = currencyType + formattedTotalBundleDiscountPrice;
        }

        //Format total bundle price atome with comma
        if (parseFloat(totalBundlePriceAtome) >= 1000){
            formattedDisplayTotalBundlePriceAtome = formattedTotalBundlePriceAtome.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            formattedDisplayTotalBundlePriceAtome = currencyType + formattedDisplayTotalBundlePriceAtome;
        }else{
            formattedDisplayTotalBundlePriceAtome = currencyType + formattedTotalBundlePriceAtome;
        }

        //Format total bundles saved price with comma
        if (parseFloat(totalBundlesSavedPrice) >= 1000){
            formattedDisplayTotalBundlesSavedPrice = formattedTotalBundlesSavedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            formattedDisplayTotalBundlesSavedPrice = currencyType + formattedDisplayTotalBundlesSavedPrice;
        }else{
            formattedDisplayTotalBundlesSavedPrice = currencyType + formattedTotalBundlesSavedPrice;
        }

        //Format total bundle saved price with comma
        if (parseFloat(totalBundleSavedPrice) >= 1000){
            formattedDisplayTotalBundleSavedPrice = formattedTotalBundleSavedPrice.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            formattedDisplayTotalBundleSavedPrice = currencyType + formattedDisplayTotalBundleSavedPrice;
        }else{
            formattedDisplayTotalBundleSavedPrice = currencyType + formattedTotalBundleSavedPrice;
        }

        if(totalBundleSavedPrice <= 0){
            totalBundleSavedPrice = 0;
            bundleSavedPriceElement.addClass('d-none').removeClass('d-block');
            bundleDiscountPriceElement.addClass('d-none').removeClass('d-block');
            $('#' + bundleVariantId + '_' + loopId + '.bundle-price-variants').addClass('mb-1');
        }else{
            bundleSavedPriceElement.removeClass('d-none');
            bundleDiscountPriceElement.removeClass('d-none');
            $('#' + bundleVariantId + '_' + loopId + '.bundle-price-variants').removeClass('mb-1');
        }

        if(totalBundlesSavedPrice <= 0){
            bundlesSavedPriceElement.html('Grab This Amazing Bundle Now! 🔥');
        }else{
            bundlesSavedPriceElement.html('<b>Save ' + formattedDisplayTotalBundlesSavedPrice + '</b> With This Bundle! 🔥');
        }

        // Set the price
        productBundleDiscountPriceElement.text(formattedDisplayProductBundleDiscountPrice);
        productBundlePriceElement.text(formattedDisplayProductBundlePrice);
        if(totalBundlesPrice >= productBundleDiscountPrice){
            productBundleDiscountPriceElement.addClass('d-none');
        }else{    
            productBundleDiscountPriceElement.removeClass('d-none');
        }
        bundlePriceElementAtome.text(formattedDisplayTotalBundlePriceAtome);
        bundlePriceElement.text(formattedDisplayBundleTotalPrice);
        bundleSavedPriceElement.text('(Save ' + formattedDisplayTotalBundleSavedPrice + ')');

        // If variant is available and have discount price, set the price. Otherwise clear the text.
        if(formattedBundleDiscountPrice){
            bundleDiscountPriceElement.text(formattedDisplayTotalBundleDiscountPrice);
            bundleDiscountPriceElement.removeClass('mr-0');
        }else{
            bundleDiscountPriceElement.text('');
            bundleDiscountPriceElement.addClass('mr-0');
        }
}

function calculateTotalVisibleComparePrice(){
    var bundlesComparePrice = 0;
    $('.single-bundle-selector__select.select-bundle').each(function(){
        var bundleVariant = $(this).val(),
        loopId = $(this).prev().data('loop'),
        selectedBundleVariant = $('#variant_' + bundleVariant + '_' + loopId),
        bundleVariantDiscPrice = selectedBundleVariant.data('discount-price'),
        bundleDiscPrice, currentBundleQuantity,
        formattedBundleVariantDiscPrice;
        if(!bundleVariantDiscPrice || bundleVariantDiscPrice == ''){
            selectedBundleVariant = $('#variant_' + $(this).find('option:nth-child(2)').val() + '_' + loopId);
            bundleVariantDiscPrice = selectedBundleVariant.data('discount-price');
        }
        currentBundleQuantity = $('input[data-product-id="' + selectedBundleVariant.parent().data('product-id') + '"][data-loop="' + loopId + '"].bundle-quantity-form').val();
        formattedBundleVariantDiscPrice = bundleVariantDiscPrice.replace(/^\D+/g, '').replace(/,/g, '');
        bundleDiscPrice = formattedBundleVariantDiscPrice * currentBundleQuantity;
        bundlesComparePrice += bundleDiscPrice;
    });
    return bundlesComparePrice;
}

function calculateTotalVisiblePrice(){
    var bundlePriceAtome = 0;
    $('.single-bundle-selector__select.select-bundle').each(function(){
        var bundleVariant = $(this).val(),
        loopId = $(this).prev().data('loop'),
        selectedBundleVariant = $('#variant_' + bundleVariant + '_' + loopId),
        bundleVariantPrice = selectedBundleVariant.data('price'),
        currentBundleQuantity, formattedBundleVariantPrice;
        if(!bundleVariantPrice || bundleVariantPrice == ''){
            selectedBundleVariant = $('#variant_' + $(this).find('option:nth-child(2)').val() + '_' + loopId);
            bundleVariantPrice = selectedBundleVariant.data('price');
        }
        currentBundleQuantity = $('input[data-product-id="' + selectedBundleVariant.parent().data('product-id') + '"][data-loop="' + loopId + '"].bundle-quantity-form').val(),
        formattedBundleVariantPrice = bundleVariantPrice.replace(/^\D+/g, '').replace(/,/g, '');
        bundlePriceAtome += (formattedBundleVariantPrice * currentBundleQuantity);
    });
    return bundlePriceAtome;
}

function calculateSavedPrice(){
    var bundleSavedPrice = 0, totalBundleSavedPrice = 0;
    $('.single-bundle-selector__select.select-bundle').each(function(){
        var bundleVariant = $(this).val(),
        loopId = $(this).prev().data('loop'),
        selectedBundleVariant = $('#variant_' + bundleVariant + '_' + loopId),
        formattedSelectedBundleVariantDiscountPrice, formattedSelectedBundleVariantPrice,
        formattedSelectedBundleVariantDiscountPriceText, formattedSelectedBundleVariantPriceText,
        selectedBundleVariantPrice = selectedBundleVariant.data('price'),
        selectedBundleVariantDiscountPrice = selectedBundleVariant.data('discount-price'),
        currentBundleQuantity;
        if(!bundleVariant || bundleVariant == ''){
            selectedBundleVariant = $('#variant_' + $(this).find('option:nth-child(2)').val() + '_' + loopId);
            selectedBundleVariantPrice = selectedBundleVariant.data('price');
            selectedBundleVariantDiscountPrice = selectedBundleVariant.data('discount-price');
        }
        currentBundleQuantity = $('input[data-product-id="' + selectedBundleVariant.parent().data('product-id') + '"][data-loop="' + loopId + '"].bundle-quantity-form').val(),
        formattedSelectedBundleVariantDiscountPrice = selectedBundleVariantDiscountPrice.replace(/^\D+/g, '').replace(/,/g, '');
        formattedSelectedBundleVariantPrice = selectedBundleVariantPrice.replace(/^\D+/g, '').replace(/,/g, '');

        formattedSelectedBundleVariantDiscountPriceText =  formattedSelectedBundleVariantDiscountPrice * currentBundleQuantity;
        formattedSelectedBundleVariantPriceText = formattedSelectedBundleVariantPrice * currentBundleQuantity;

        bundleSavedPrice = (formattedSelectedBundleVariantDiscountPriceText - formattedSelectedBundleVariantPriceText);
        totalBundleSavedPrice += bundleSavedPrice;
    });
    return totalBundleSavedPrice;
}

function clearForm(){
    $('#product-options').find('input[type="radio"], select').each(function(){
        var currentElement = $(this);

        if(currentElement.is(':radio')){
            currentElement.prop('checked', false);
        }else{
            currentElement.prop('selectedIndex',0);
        }
    });
}

function enableButtons(){
    var btn, text,
        svg_cart_icon = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>';

    $("#btn-buynow, #btn-addtocart").each(function(){
        btn = $(this);
        text = btn.is('#btn-buynow')? 'Buy now' : svg_cart_icon + ' Add to cart';

        btn.removeAttr('disabled', 'disabled');
        btn.text(''); // clear first

        {% if settings.options.buy_now_btn_txt %}
        text = btn.is('#btn-buynow')? '{{settings.options.buy_now_btn_txt}}' : text;
        {% endif %}

        btn.html(text);
    });
}

function disabledButtons(){
    var btn;

    $("#btn-buynow, #btn-addtocart").each(function(){
        btn = $(this);

        btn.attr('disabled', 'disabled');
        btn.text('Sold out');
	});
}

function disabledBundleButtons(){
    var btn;

    $("#btn-buynow, #btn-addtocart").each(function(){
        btn = $(this);

        btn.attr('disabled', 'disabled');
	});
}

function updateBundleAvailabilityText(productId, variantId, loopId){
    var row = $('#row_' + productId + '_' + loopId),
    span = row.find('#bundle_availability_' + variantId + '_' + loopId),
    selectedVariant = $('#variant_'+ variantId + '_' + loopId),
    availability = selectedVariant.data('available'),
    isAvailable = true;

    if(!availability){
        isAvailable = false;
    }
    
    if(isAvailable){
        span.addClass('d-none').removeClass('d-block');
        row.find('[id^="bundle_availability_"]').addClass('d-none').removeClass('d-block');
    }else{
        row.find('[id^="bundle_availability_"]').addClass('d-none').removeClass('d-block');
        span.removeClass('d-none').addClass('d-block');
    }
}

function isUnlimitedVariant(){
    var isManageStock, isUnlimited,
        variantId = $('input#selected-option[name="id"]').attr('value'),
        selectedVariant = $('#variant_'+ variantId);

        // get current selected variant manage stock
        isManageStock = selectedVariant.data('manage-stock');

        // check manage stock boolean, if false, means it unlimited stock
        isUnlimited = (!isManageStock)? true : false;
        return isUnlimited;
}

function isUnlimitedBundleVariant(productId, loopId){
    var isManageStock, isUnlimited,
        variantId = $('#product_bundle_' + productId + '_variant_' + loopId).val(),
        selectedVariant = $('#variant_' + variantId + '_' + loopId ),

        // get current selected variant manage stock
        isManageStock = selectedVariant.data('manage-stock');

        // check manage stock boolean, if false, means it unlimited stock
        isUnlimited = (!isManageStock)? true : false;
        return isUnlimited;
}

function checkBundleSelect(productId, loopId){
    var input_quantity_box = $('input[data-product-id="'+ productId +'"][data-loop="'+ loopId +'"]'),
    buttonPlus = $('.bundle-plus[data-product-id="'+ productId +'"][data-loop="'+ loopId +'"]'),
    buttonMinus = $('.bundle-minus[data-product-id="'+ productId +'"][data-loop="'+ loopId +'"]'),
    inputProductId = $('.bundle-variants').find('input[value="'+ productId +'"][data-loop="'+ loopId +'"]').val(),
    selectBundleVariants = $('.bundle-variants').find('input[value="'+ productId +'"][data-loop="'+ loopId +'"]').next('select').val(),
    selectedVariant = $('#variant_' + selectBundleVariants + '_' + loopId ),
    available = false;

    if(selectBundleVariants != ''){
        available = selectedVariant.data('available');
    }

    if((selectBundleVariants == '') && (inputProductId == '') || available == false){
        buttonPlus.addClass('disabled').attr('disabled', 'disabled');
        buttonMinus.addClass('disabled').attr('disabled', 'disabled');
        input_quantity_box.prop('readonly', true);
        input_quantity_box.prop('disabled', false);
    }else{
        buttonPlus.removeClass('disabled').removeAttr('disabled', 'disabled');
        buttonMinus.removeClass('disabled').removeAttr('disabled', 'disabled');
        input_quantity_box.prop('readonly', false);
        input_quantity_box.prop('disabled', false);
    }

}

function updateQuantityText(){
    var selectedVariant = selectedVariant = $('#variant_' + $('#selected-option').val()),
        quantity = selectedVariant.attr('data-inventory-quantity'),
        max_product_text = $('#max-product-quantity');

    max_product_text.text(quantity);
    max_product_text.attr('value', quantity);
}

function showQuantityText(show = true){
    var quantity_div_text = $('#variant_quantity_text');

    if(quantity_div_text.hasClass('d-none') || !show || isUnlimitedVariant()){
        quantity_div_text.hide();
    }else{
        quantity_div_text.show();
    }
}

function resetInputQuantity(value = 1){
    var input_quantity_box = $('#product_input_quantity');

    input_quantity_box.attr('value', value);
    input_quantity_box.val(value);
}

function plusQuantity(){
    var input_quantity_box = $('#product_input_quantity'),
        current_value = parseInt($('#product_input_quantity').attr('value')),
        max_value = parseInt($('#max-product-quantity').attr('value')),
        new_value = 1,
        isUnlimited = false;

        new_value = current_value + 1;
        isUnlimited = isUnlimitedVariant();

        // if new value is not less than maximum available stock and stock is unlimited
        if((new_value < max_value) || isUnlimited == true){
            input_quantity_box.attr('value', new_value);
            input_quantity_box.val(new_value);
        }else{
            input_quantity_box.attr('value', max_value);
            input_quantity_box.val(max_value);
        }

        // Update price text after quantity changed
        updatePriceText();
}

function resetBundleInputQuantity(productId, value = 1, loopId){
    var input_quantity_box = $('input[data-product-id="'+ productId +'"][data-loop="'+ loopId +'"]');

    input_quantity_box.attr('value', value);
    input_quantity_box.val(value);
}

function plusBundleQuantity(productId, loopId){
    var input_quantity_box = $('input[data-product-id="'+ productId +'"][data-loop="'+ loopId +'"]'),
        current_value = parseInt(input_quantity_box.attr('value')),
        selectedProductVariant = $('#product_bundle_' + productId + '_variant_' + loopId).val(),
        selectedVariant = $('#variant_' + selectedProductVariant + '_' + loopId ),
        max_value = parseInt(selectedVariant.data('inventory-quantity')),
        new_value = 1,
        isUnlimited = false;

        new_value = current_value + 1;
        isUnlimited = isUnlimitedBundleVariant(productId, loopId);

        // if new value is not less than maximum available stock and stock is unlimited
        if((new_value <= max_value) || isUnlimited == true){
            input_quantity_box.attr('value', new_value);
            input_quantity_box.val(new_value);

            // Update price text after quantity changed
            updateBundlePriceText(productId, selectedProductVariant, loopId);
        }else{
            input_quantity_box.attr('value', max_value);
            input_quantity_box.val(max_value);
        }
}

function minusQuantity(){
    var input_quantity_box = $('#product_input_quantity'),
        current_value = parseInt($('#product_input_quantity').attr('value')),
        min_value = 1,
        new_value = 1;

        new_value = current_value - 1;

        if(new_value >= min_value && new_value > 0){
            input_quantity_box.attr('value', new_value);
            input_quantity_box.val(new_value);
        }

        // Update price text after quantity changed
        updatePriceText();
}

function minusBundleQuantity(productId, loopId){
    var input_quantity_box = $('input[data-product-id="'+ productId +'"][data-loop="'+ loopId +'"]'),
        current_value = parseInt(input_quantity_box.attr('value')),
        selectedProductVariant = $('#product_bundle_' + productId + '_variant_' + loopId).val(),
        min_value = 1,
        new_value = 1;

        new_value = current_value - 1;

        if(new_value >= min_value && new_value > 0){
            input_quantity_box.attr('value', new_value);
            input_quantity_box.val(new_value);
        }

        // Update price text after quantity changed
        updateBundlePriceText(productId, selectedProductVariant, loopId);
}

function inputQuantity(event){
    var input_quantity_box = $('#product_input_quantity'),
        current_value = parseInt($('#product_input_quantity').attr('value')),
        max_value = parseInt($('#max-product-quantity').attr('value')),
        input_value = 1,
        isUnlimited = false;

        isUnlimited = isUnlimitedVariant(); 

        input_value = input_quantity_box.val();
        input_value = input_value.replace(/[^0-9\.]/g,''); // regex, which allow number only
        input_value = parseInt(input_value);

        if(input_quantity_box.val()){
            if((input_value < max_value) || isUnlimited){
                input_quantity_box.attr('value', input_value);
                input_quantity_box.val(input_value);
            }else{
                input_quantity_box.attr('value', max_value);
                input_quantity_box.val(max_value);
            }
        }

        // Update price text after quantity changed
        updatePriceText();
}

function inputBundleQuantity(productId, loopId){
    var input_quantity_box = $('input[data-product-id="'+ productId +'"][data-loop="'+ loopId +'"]'),
        selectedProductVariant = $('#product_bundle_' + productId + '_variant_' + loopId).val(),
        selectedVariant = $('#variant_' + selectedProductVariant + '_' + loopId ),
        max_value = parseInt(selectedVariant.data('inventory-quantity')),
        input_value = 1,
        isUnlimited = false;

        isUnlimited = isUnlimitedVariant(); 

        input_value = input_quantity_box.val();
        input_value = input_value.replace(/[^0-9\.]/g,''); // regex, which allow number only
        input_value = parseInt(input_value);

        if(input_quantity_box.val()){
            if((input_value < max_value) || isUnlimited){
                input_quantity_box.attr('value', input_value);
                input_quantity_box.val(input_value);
            }else{
                input_quantity_box.attr('value', max_value);
                input_quantity_box.val(max_value);
            }
        }

        // Update price text after quantity changed
        updateBundlePriceText(productId, selectedProductVariant, loopId);
}